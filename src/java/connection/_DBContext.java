package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.Helper;

public class _DBContext {

    protected Connection connection;

    private final byte env = 0;
    private final Properties properties = Helper.getPropertiesByFileName("constant/const.properties");

    private String hostname, port, name, username, password;

    public _DBContext() {
        String _envStr = "";
        if (env == 0) {
            _envStr = "local-";
        }

        hostname = properties.getProperty(_envStr + "database.hostname");
        port = properties.getProperty(_envStr + "database.port");
        name = properties.getProperty(_envStr + "database.name");
        username = properties.getProperty(_envStr + "database.username");
        password = properties.getProperty(_envStr + "database.password");

        try {
            String user = "root";
            String pass = "long240303";
            String url = "jdbc:mysql://localhost:3306/ita301";
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(url, user, pass);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(_DBContext.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Connection getConnection() {
        return connection;
    }

}
