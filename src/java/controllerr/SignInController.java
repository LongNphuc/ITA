package controllerr;

//import dao.UserDAO;
import dao.UserDAO;
import dto.UserGoogleDTO;
import java.io.IOException;

//import dto.UserGoogleDTO;
//import utils.Helper;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.User;
import utils.Helper;

@WebServlet(name = "SignInController", urlPatterns = {"/LoginController"})
public class SignInController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("signin") != null) {
            request.getRequestDispatcher("sign-in.jsp").forward(request, response);
        } else if (request.getParameter("account") != null) {
            String account = request.getParameter("account");
            String password = request.getParameter("password");
            try {
                password = Helper.encryptPassword(password);
            } catch (Exception e) {
                e.printStackTrace();
            }
            boolean flag = false;
            for (User user : new UserDAO().getUserList(false)) {
                if (account.contains("@")) {
                    if (user.getEmail().equals(account.trim()) && user.getPassword().equals(password)) {
                        request.getSession().setAttribute("user", user);
                        flag = true;
                    }
                } else {
                    if (user.getMobile().equals(account.trim()) && user.getPassword().equals(password)) {
                        request.getSession().setAttribute("user", user);
                        flag = true;
                    }
                }
            }
            if (flag) {
                response.sendRedirect(request.getContextPath());
                return;
            }else {
                request.setAttribute("mess", (account.contains("@") ? "Email "
                        : "Mobile ") +" not exist! Or password not correct!");
                request.getRequestDispatcher("sign-in.jsp").forward(request, response);
            }
        } else {
            String code = request.getParameter("code");
            String accessToken = Helper.getToken(code);

            UserGoogleDTO userInfo = Helper.getUserInfo(accessToken);
            if (request.getSession().getAttribute("checkExist") != null) {
                if (new UserDAO().isUserExist(UserDAO.LOGIN_EMAIL, userInfo.getEmail())) {
                    request.getSession().setAttribute("mess", "Email had been register!");
                    response.sendRedirect(request.getContextPath() + "/sign-up?signup=true");
                } else {
                    request.setAttribute("userGoogle", userInfo);
                    request.getRequestDispatcher("userGoogleFillInfo.jsp").forward(request, response);
                }
            }else {
                for (User user : new UserDAO().getUserList(false)) {
                    if(user.getEmail().equals(userInfo.getEmail()) && user.getOauthId().equals(userInfo.getId())) {
                        request.getSession().setAttribute("user", user);
                    }
                }System.out.println(request.getContextPath());
                response.sendRedirect(request.getContextPath());
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
